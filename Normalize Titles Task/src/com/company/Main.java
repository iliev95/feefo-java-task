package com.company;

import com.company.normalization.Normalizer;
import com.company.services.JobTitleService;

import java.util.Scanner;

public class Main {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    String inputText = scanner.nextLine();
    JobTitleService jobTitleService = new JobTitleService();

    while (!inputText.equalsIgnoreCase("EXIT")) {
      String result = jobTitleService.normalizeInputAgainstJobTitles(inputText);
      System.out.println(result);
      inputText = scanner.nextLine();
    }
  }
}
