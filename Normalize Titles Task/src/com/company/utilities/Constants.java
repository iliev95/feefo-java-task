package com.company.utilities;

import java.util.List;

public class Constants {

  // Available job titles
  public static final String ARCHITECT = "Architect";
  public static final String SOFTWARE_ENGINEER = "Software engineer";
  public static final String QUANTITY_SURVEYOR = "Quantity surveyor";
  public static final String ACCOUNTANT = "Accountant";

  // Final List of job titles
  public static final List<String> JOB_TITLES = List.of(ARCHITECT, SOFTWARE_ENGINEER, QUANTITY_SURVEYOR, ACCOUNTANT);

  // Edge case messages
  public static final String INVALID_INPUT = "Input is empty. Please enter a valid text";
  public static final String NO_FOUND_MATCHES = "No matches were found for this input.";
}
