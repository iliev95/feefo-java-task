package com.company.utilities;

public class StringUtils {

  public static String formatInitialText(String input) {
    String result = input.replaceAll("\\s+", "");
    return result.toLowerCase();
  }
}
