package com.company.normalization;

import java.util.List;

import static com.company.utilities.Constants.INVALID_INPUT;
import static com.company.utilities.Constants.NO_FOUND_MATCHES;
import static com.company.utilities.StringUtils.formatInitialText;

/**
 * The purpose of the class is to present a way of normalizing
 * a single {@link String} value against a single/list of String values.
 * The user has two options, either normalize a string against a
 * single value or against a list of values, finding the closest
 * match. Both linear and Levenshtein's {@link String} comparison have been
 * implemented.
 */
public class Normalizer {

  public Normalizer() {
  }

  /**
   * Purpose of the method is to normalize an input text against a list of strings.
   *
   * @param inputText        first string to compare
   * @param compareToStrings a list of string values to compare to
   * @return the quality score, resulting from the comparison
   */
  public String normalize(String inputText, List<String> compareToStrings) {

    if (inputText == null || inputText.isEmpty()) {
      return INVALID_INPUT;
    }

    String formattedInput = formatInitialText(inputText);
    double highestQualityScore = Double.MIN_VALUE;
    String result = "";

    for (String compareToText : compareToStrings) {
      String formattedJobTitle = formatInitialText(compareToText);
      double currentQualityScore = compareStringsToQualityScore(formattedInput, formattedJobTitle);

      if (currentQualityScore != 0.0 && currentQualityScore > highestQualityScore) {
        highestQualityScore = currentQualityScore;
        result = compareToText;
      }
    }

    // This value may be changed. Decided to put it here, because having a match of
    // for example: 0.1 might flood an eventual database with false values.
    if (highestQualityScore < 0.3) {
      return NO_FOUND_MATCHES;
    }

    return result;
  }

  /**
   * Purpose of the method is to normalize an input text against single String value.
   *
   * @param inputText       first string to compare
   * @param compareToString astring value to compare to
   * @return the quality score, resulting from the comparison
   */
  public String normalize(String inputText, String compareToString) {

    if (inputText == null || inputText.isEmpty()) {
      return INVALID_INPUT;
    }

    String formattedInput = formatInitialText(inputText);
    String formattedJobTitle = formatInitialText(compareToString);

    String result = "";
    double currentQualityScore = compareStringsToQualityScore(formattedInput, formattedJobTitle);
    if (currentQualityScore != 0.0) {
      result = compareToString;
    }
    return result;
  }

  /**
   * Purpose of the method is to normalize an input text against a list of strings.
   *
   * @param input         first string to compare
   * @param compareToText a list of string values to compare to
   * @return the quality score, resulting from the comparison
   */
  public double compareStringsToQualityScore(String input, String compareToText) {
    String longer = input;
    String shorter = compareToText;
    if (input.length() < compareToText.length()) {
      longer = compareToText;
      shorter = input;
    }

    int longerLength = longer.length();
    if (longerLength == 0) {
      return 1.0;
    }
    return (longerLength - compareStringToQualityScoreLevenshtein(longer, shorter)) / (double) longerLength;
  }

  /**
   * Purpose of the method is comparing two strings, using the Levenshtein
   * algorithm.
   *
   * @param input         first string to compare
   * @param compareToText second string to compare to
   * @return the quality score, resulting from the comparison
   */
  public double compareStringToQualityScoreLevenshtein(String input, String compareToText) {
    int[] changeOperations = new int[compareToText.length() + 1];
    for (int i = 0; i <= input.length(); i++) {
      int lastValue = i;
      for (int j = 0; j <= compareToText.length(); j++) {
        if (i == 0)
          changeOperations[j] = j;
        else {
          if (j > 0) {
            int newValue = changeOperations[j - 1];
            if (input.charAt(i - 1) != compareToText.charAt(j - 1))
              newValue = Math.min(Math.min(newValue, lastValue),
                  changeOperations[j]) + 1;
            changeOperations[j - 1] = lastValue;
            lastValue = newValue;
          }
        }
      }
      if (i > 0)
        changeOperations[compareToText.length()] = lastValue;
    }
    return changeOperations[compareToText.length()];
  }

  /**
   * Purpose of this method is comparing the two strings in a linear fashion.
   * This might serve as a simpler solution, in case we are not looking for
   * a
   *
   * @param input         first string to compare
   * @param compareToText second string to compare to
   * @return the quality score, resulting from the comparison
   */
  public double compareStringToQualityScoreLinear(String input, String compareToText) {
    int matchingCharacters = compareCharacters(input, compareToText);
    int totalMatches = compareCharacters(input, input);
    return matchingCharacters / (double) totalMatches;
  }

  /**
   * Purpose of this method is to calculate the count of matching
   * chgaracters in both strings.
   *
   * @param input         first string to compare
   * @param textToCompare second string to compare to
   * @return the quality score, resulting from the comparison
   */
  public int compareCharacters(String input, String textToCompare) {
    char[] inputArray = input.toCharArray();
    char[] textToCompareArray = textToCompare.toCharArray();

    int matchesCount = 0;

    for (int i = 0; i < inputArray.length; i++) {
      for (int j = 0; j < textToCompareArray.length; j++) {
        char currentInputChar = inputArray[i];
        if (currentInputChar == textToCompareArray[j]) {
          matchesCount++;
        }
      }
    }
    return matchesCount;
  }
}
