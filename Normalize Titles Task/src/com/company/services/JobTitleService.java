package com.company.services;

import com.company.normalization.Normalizer;
import static com.company.utilities.Constants.JOB_TITLES;
import static com.company.utilities.Constants.NO_FOUND_MATCHES;

/**
 * The purpose of the class is to separate task's business logic, in favor
 * of keeping the {@link Normalizer} class generic and not dependent on
 * any business logic.
 */
public class JobTitleService {

  private Normalizer normalizer;

  public JobTitleService() {
    this.normalizer = new Normalizer();
  }

  public String normalizeInputAgainstJobTitles(String input) {
    String result = normalizer.normalize(input, JOB_TITLES);
    return (result == null || result.isEmpty()) ? NO_FOUND_MATCHES : result;
  }
}
